% hist_eq2.m
% Histogram Equalization 直方图均衡化
% 使用histeq函数
% 参考 https://wenku.baidu.com/view/ee793d64caaedd3382c4d303.html

close all;
clear all;
clc;

RGB = imread('1.JPG'); % 读取彩色图

R = RGB(:,:,1);
G = RGB(:,:,2);
B = RGB(:,:,3);

r = histeq(R);
g = histeq(G);
b = histeq(B);
RGB_he = cat(3,r,g,b);

subplot(121);imshow(RGB);title('原图');
subplot(122);imshow(RGB_he);title('直方图均衡处理后');

figure
subplot(3,2,1),imhist(R);title('真彩色图像的红色分量直方图');
subplot(3,2,3),imhist(G);title('真彩色图像的绿色分量直方图');
subplot(3,2,5),imhist(B);title('真彩色图像的蓝色分量直方图');
subplot(3,2,2),imhist(r);title('彩色图像的红色分量直方图');
subplot(3,2,4),imhist(g);title('彩色图像的绿色分量直方图');
subplot(3,2,6),imhist(b);title('彩色图像的蓝色分量直方图');

