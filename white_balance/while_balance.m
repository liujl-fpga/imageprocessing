%%白平衡与色温紧密相关，不同色温光源下图像会呈现不同程度的偏色
%%由于人眼独特的适应性，在不同光照条件下观看物体时不会出现偏色，而就没这么先进了
%%蓝色光色温高，红色光色温低
 
clc;
clear all;
close all;
tic;
%imgSrc = imread('E:\picture\03-work\02-imgProc\00-ISP\wb_sardmen-incorrect.jpg');
imgSrc = imread('.\1.jpg');
imgDst = imgSrc;
%%第一步，计算三个通道的平均灰度
imgR = imgSrc(:,:,1);
imgG = imgSrc(:,:,2);
imgB = imgSrc(:,:,3);
RAve = mean2(imgR);
GAve = mean2(imgG);
BAve = mean2(imgB);
aveGray = (RAve + GAve + BAve) / 3;
%%第二步，计算三个通道的增益系数
RCoef = aveGray / RAve;
GCoef = aveGray / GAve;
BCoef = aveGray / BAve;
%%第三步，使用增益系数来调整原始图像
RCorrection = RCoef * imgR;
GCorrection = GCoef * imgG;
BCorrection = BCoef * imgB;
imgDst(:,:,1) = RCorrection;
imgDst(:,:,2) = GCorrection;
imgDst(:,:,3) = BCorrection;
figure,subplot(1,2,1),imshow(imgSrc),title('original image');
subplot(1,2,2),imshow(imgDst),title('white balanced image');
toc;

%————————————————
%版权声明：本文为CSDN博主「lemonHe_」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
%原文链接：https://blog.csdn.net/helimin12345/article/details/78255669